package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.Tag;

import java.util.ArrayList;

public interface TagMapper {
    public ArrayList<Tag> getAllTags();
    public ArrayList<Tag> getArticleTags(@Param("article_id") Integer article_id);
    public Tag getTagById(@Param("id") Integer id);
    public Tag getTagByName(String tname);
    public void addTag(Tag tag);
    public void updateTag(Tag tag);
    public void deleteTagById(@Param("id") Integer id);
}

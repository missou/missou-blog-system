package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.Comment;

import java.util.ArrayList;


public interface CommentMapper {
    public void insertComment(Comment comment);
    public void deleteCommentById(@Param("id") Integer id);
    public ArrayList<Comment> getArticleComments(@Param("article_id") Integer article_id);
    public ArrayList<Comment> getAllComments();
}

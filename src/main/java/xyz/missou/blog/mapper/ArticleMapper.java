package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.MonthArticle;

import java.util.ArrayList;

public interface ArticleMapper {
    public ArrayList<Article> getArticles(@Param("category_id") Integer category_id,@Param("tag_id") Integer tag_id,@Param("hot") boolean hot);
    public ArrayList<Article> getArticlesByCategoryIdMin(@Param("category_id") Integer category_id);
    public ArrayList<MonthArticle> getAllMonthArticle();
    public ArrayList<Article> getAllMonthArticle(@Param("create_month") String create_month);
    public Article getArticleById(@Param("id") Integer id);
    public Article getArticleByIdMin(@Param("id") Integer id);
    public void pageViewUpdate(@Param("id") Integer id);
    public void deleteArticleById(@Param("id") Integer id);
    public void addArticle(Article article);
    public void updateArticle(Article article);
}

package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.User;

import java.util.ArrayList;

public interface UserMapper {
    public User getUserByAccount(@Param("account") String account);
    public void insertUser(User user);
    public User verifyUser(@Param("account") String account,@Param("password")  String password);
    public User getUserByIdMin(@Param("id") Integer id);
    public ArrayList<User> getAllUsersMin();
    public void deleteUserById(@Param("id") Integer id);
    public void updateUser(User newuser);
}

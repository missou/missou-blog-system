package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.ArticleTag;
import xyz.missou.blog.entity.MonthArticle;

import java.util.ArrayList;

public interface ArticleTagMapper {
    public void addArticleTag(ArticleTag articleTag);
    public void deleteByArticleId(@Param("article_id") Integer article_id);
}

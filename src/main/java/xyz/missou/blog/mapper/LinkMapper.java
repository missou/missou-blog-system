package xyz.missou.blog.mapper;

import xyz.missou.blog.entity.Link;

import java.util.ArrayList;

public interface LinkMapper {
    public ArrayList<Link> getAllLinks();
}

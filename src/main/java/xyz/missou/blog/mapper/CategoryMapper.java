package xyz.missou.blog.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.missou.blog.entity.Category;

import java.util.ArrayList;
import java.util.List;

public interface CategoryMapper {
    public ArrayList<Category> getAllCategories();
    public Category getCategoryById(@Param("id") Integer id);
    public Category getCategoryByIdMin(@Param("id") Integer id);
    public void deleteCategoryById(@Param("id") Integer id);
    public Category getCategoryByName(@Param("name") String name);
    public void updateCategory(Category category);
    public void addCategory(Category category);
}

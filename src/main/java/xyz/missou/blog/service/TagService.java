package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Tag;
import xyz.missou.blog.mapper.TagMapper;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class TagService {

    @Resource
    private TagMapper tagMapper;

    public ArrayList<Tag> getAllTags() {
        return tagMapper.getAllTags();
    }
    public ArrayList<Tag> getArticleTags(Integer article_id) {
        return tagMapper.getArticleTags(article_id);
    }
    public Tag getTagById(Integer id) {
        return tagMapper.getTagById(id);
    }

    public Tag getTagByName(String tname) {
        return tagMapper.getTagByName(tname);
    }

    public void addTag(Tag tag) {
        tagMapper.addTag(tag);
    }

    public void deleteTagById(Integer id) {
        tagMapper.deleteTagById(id);
    }

    public void updateTag(Tag tag) {
        tagMapper.updateTag(tag);
    }
}

package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.User;
import xyz.missou.blog.mapper.UserMapper;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    public User verifyUser(String account, String password) {
        return userMapper.verifyUser(account, password);
    }

    public User getUserByAccount(String account) {
        return userMapper.getUserByAccount(account);
    }

    public boolean havaUser(String account) {
        if (userMapper.getUserByAccount(account)!=null)
            return true;
        return false;
    }

    public void addUser(User user) {
        userMapper.insertUser(user);
    }

    public ArrayList<User> getAllUsersMin() {
        return userMapper.getAllUsersMin();
    }

    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }

    public void updateUser(User newuser) {
        userMapper.updateUser(newuser);
    }
}

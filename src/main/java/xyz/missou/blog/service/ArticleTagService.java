package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.ArticleTag;
import xyz.missou.blog.entity.MonthArticle;
import xyz.missou.blog.mapper.ArticleTagMapper;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class ArticleTagService {

    @Resource
    private ArticleTagMapper articleTagMapper;

    public void addArticleTags(ArrayList<ArticleTag> articleTags) {
        for (ArticleTag articleTag:articleTags){
            articleTagMapper.addArticleTag(articleTag);
        }
    }
    public void deleteByArticleId(Integer articleId) {
        articleTagMapper.deleteByArticleId(articleId);
    }
}

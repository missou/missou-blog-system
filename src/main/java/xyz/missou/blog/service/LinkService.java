package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Link;
import xyz.missou.blog.mapper.LinkMapper;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class LinkService {

    @Resource
    private LinkMapper linkMapper;

    public ArrayList<Link> getAllLinks() {
        return linkMapper.getAllLinks();
    }
}

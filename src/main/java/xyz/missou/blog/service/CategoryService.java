package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Category;
import xyz.missou.blog.mapper.CategoryMapper;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Resource
    private CategoryMapper categoryMapper;

    public ArrayList<Category> getAllCategories(){
        return categoryMapper.getAllCategories();
    }
    public Category getCategoryById(Integer id){
        return categoryMapper.getCategoryById(id);
    }

    public Category getCategoryByIdMin(Integer category) {
        return categoryMapper.getCategoryByIdMin(category);
    }

    public void deleteCategoryById(Integer id) {
        categoryMapper.deleteCategoryById(id);
    }

    public Category getCategoryByName(String name) {
        return categoryMapper.getCategoryByName(name);
    }

    public void updateCategory(Category category) {
        categoryMapper.updateCategory(category);
    }

    public void addCategory(Category category) {
        categoryMapper.addCategory(category);
    }
}

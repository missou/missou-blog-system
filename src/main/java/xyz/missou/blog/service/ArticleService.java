package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.MonthArticle;
import xyz.missou.blog.mapper.ArticleMapper;

import javax.annotation.Resource;
import java.util.ArrayList;

@Service
public class ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    public ArrayList<Article> getAllArticles(boolean hot) {
        return articleMapper.getArticles(null,null,hot);
    }
    public ArrayList<Article> getArticlesByCategoryId(Integer category_id,boolean hot) {
        return articleMapper.getArticles(category_id,null,hot);
    }
    public ArrayList<MonthArticle> getAllMonthArticles() {
        return articleMapper.getAllMonthArticle();
    }
    public ArrayList<Article> getArticlesByTagId(Integer tag_id,boolean hot) {
        return articleMapper.getArticles(null,tag_id,hot);
    }
    public Article getArticleById(Integer id) {
        return articleMapper.getArticleById(id);
    }
    public void pageViewUpdate(Integer id){
        articleMapper.pageViewUpdate(id);
    }

    public void addArticle(Article article) {
        articleMapper.addArticle(article);
    }

    public void deleteArticleById(Integer id){
        articleMapper.deleteArticleById(id);
    }

    public void updateArticle(Article article) {
        articleMapper.updateArticle(article);
    }
}

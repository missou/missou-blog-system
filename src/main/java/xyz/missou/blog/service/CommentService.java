package xyz.missou.blog.service;

import org.springframework.stereotype.Service;
import xyz.missou.blog.entity.Comment;
import xyz.missou.blog.mapper.CommentMapper;

import javax.annotation.Resource;
import java.util.ArrayList;


@Service
public class CommentService {

    @Resource
    private CommentMapper commentMapper;

    public void addComment(Comment comment){
        commentMapper.insertComment(comment);
    }

    public void deleteCommentById(Integer id){
        commentMapper.deleteCommentById(id);
    }

    public ArrayList<Comment> getArticleComments(Integer article_id){
        return commentMapper.getArticleComments(article_id);
    }

    public ArrayList<Comment> getAllComments(){
        return commentMapper.getAllComments();
    }

}

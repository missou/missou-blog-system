package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.missou.blog.entity.*;
import xyz.missou.blog.service.*;

import javax.annotation.Resource;
import java.util.ArrayList;

@Controller
@RequestMapping("/manage")
public class ManageController extends BaseController{

    @Resource
    private UserService userService;
    @Resource
    private TagService tagService;
    @Resource
    private CategoryService categoryService;
    @Resource
    private ArticleService articleService;
    @Resource
    private CommentService commentService;

    @RequestMapping({"/",""})
    public String index(){
        return "redirect:/manage/article/";
    }

    @RequestMapping({"/user/","/user"})
    public String user(Model model, @ModelAttribute("msg") String msg){
        ArrayList<User> users=userService.getAllUsersMin();
        model.addAttribute("msg",msg);
        model.addAttribute("users",users);
        model.addAttribute("page","user");
        return view("manage/index");
    }

    @RequestMapping({"/category/","/category"})
    public String category(Model model,@ModelAttribute("msg") String msg){
        ArrayList<Category> categories=categoryService.getAllCategories();
        model.addAttribute("msg",msg);
        model.addAttribute("categories",categories);
        model.addAttribute("page","category");
        return view("manage/index");
    }

    @RequestMapping({"/tag/","/tag"})
    public String tag(Model model,@ModelAttribute("msg") String msg){
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("msg",msg);
        model.addAttribute("tags",tags);
        model.addAttribute("page","tag");
        return view("manage/index");
    }

    @RequestMapping({"/article/","/article"})
    public String article(Model model){
        ArrayList<Article> articles=articleService.getAllArticles(false);
        model.addAttribute("articles",articles);
        model.addAttribute("page","article");
        return view("manage/index");
    }

    @RequestMapping({"/comment/","/comment"})
    public String comment(Model model){
        ArrayList<Comment> comments=commentService.getAllComments();
        model.addAttribute("comments",comments);
        model.addAttribute("page","comment");
        return view("manage/index");
    }
}

package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.missou.blog.entity.*;
import xyz.missou.blog.service.ArticleService;
import xyz.missou.blog.service.CategoryService;
import xyz.missou.blog.service.LinkService;
import xyz.missou.blog.service.TagService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
public class IndexController extends BaseController{

    @Resource
    private CategoryService categoryService;
    @Resource
    private ArticleService articleService;
    @Resource
    private LinkService linkService;
    @Resource
    private TagService tagService;

    @RequestMapping({"/",""})
    public String index(HttpServletRequest request, Model model){
        ArrayList<Article> articles=articleService.getAllArticles(false);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actItem","index");
        return view("index");
    }
    @RequestMapping({"/hot/","/hot"})
    public String indexHot(HttpServletRequest request, Model model){
        ArrayList<Article> articles=articleService.getAllArticles(true);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actItem","index");
        model.addAttribute("hot",true);
        return view("index");
    }

    @RequestMapping({"/archive/","/archive"})
    public String archive(HttpServletRequest request, Model model){
        ArrayList<MonthArticle> monthArticles=articleService.getAllMonthArticles();
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("monthArticles",monthArticles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actItem","archive");
        return view("archive");
    }
    @RequestMapping({"/about/","/about"})
    public String about(HttpServletRequest request, Model model){
        ArrayList<MonthArticle> monthArticles=articleService.getAllMonthArticles();
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("monthArticles",monthArticles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actItem","about");
        return view("about");
    }

}

package xyz.missou.blog.controller;

public class BaseController {

    private static String theme="default";

    public String view(String path) {
        return "themes/"+theme+"/"+path;
    }
}

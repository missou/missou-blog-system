package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageNotFoundController extends BaseController{

    @RequestMapping({"/404/","/404"})
    public String article(){
        return view("404");
    }
}

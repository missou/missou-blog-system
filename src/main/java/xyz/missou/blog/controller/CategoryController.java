package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.Category;
import xyz.missou.blog.entity.Link;
import xyz.missou.blog.entity.Tag;
import xyz.missou.blog.service.ArticleService;
import xyz.missou.blog.service.CategoryService;
import xyz.missou.blog.service.LinkService;
import xyz.missou.blog.service.TagService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
public class CategoryController extends BaseController{

    @Resource
    private CategoryService categoryService;
    @Resource
    private ArticleService articleService;
    @Resource
    private LinkService linkService;
    @Resource
    private TagService tagService;

    @RequestMapping({"/category/{category_id}/","/category/{category_id}"})
    public String category(HttpServletRequest request, Model model,@PathVariable("category_id") Integer category_id){
        ArrayList<Article> articles=articleService.getArticlesByCategoryId(category_id,false);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        Category actCategory=categoryService.getCategoryById(category_id);
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actCategory",actCategory);
        return view("category_article");
    }
    @RequestMapping({"/category/{category_id}/hot/","/category/{category_id}/hot"})
    public String categoryHot(HttpServletRequest request, Model model,@PathVariable("category_id") Integer category_id){
        ArrayList<Article> articles=articleService.getArticlesByCategoryId(category_id,true);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("hot",true);
        model.addAttribute("actCategory",categoryService.getCategoryById(category_id));
        return view("category_article");
    }
}

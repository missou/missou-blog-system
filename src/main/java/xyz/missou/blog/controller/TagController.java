package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.missou.blog.entity.Article;
import xyz.missou.blog.entity.Category;
import xyz.missou.blog.entity.Link;
import xyz.missou.blog.entity.Tag;
import xyz.missou.blog.service.ArticleService;
import xyz.missou.blog.service.CategoryService;
import xyz.missou.blog.service.LinkService;
import xyz.missou.blog.service.TagService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
@RequestMapping("/tag")
public class TagController extends BaseController{

    @Resource
    private CategoryService categoryService;
    @Resource
    private ArticleService articleService;
    @Resource
    private LinkService linkService;
    @Resource
    private TagService tagService;

    @RequestMapping({"/{tag_id}/","/{tag_id}"})
    public String category(HttpServletRequest request, Model model, @PathVariable("tag_id") Integer tag_id){
        ArrayList<Article> articles=articleService.getArticlesByTagId(tag_id,false);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("actTag",tagService.getTagById(tag_id));
        return view("tag_article");
    }
    @RequestMapping({"/{tag_id}/hot/","/{tag_id}/hot"})
    public String categoryHot(HttpServletRequest request, Model model,@PathVariable("tag_id") Integer tag_id){
        ArrayList<Article> articles=articleService.getArticlesByTagId(tag_id,true);
        ArrayList<Category> categories= categoryService.getAllCategories();
        ArrayList<Link> links=linkService.getAllLinks();
        ArrayList<Tag> tags=tagService.getAllTags();
        model.addAttribute("articles",articles);
        model.addAttribute("categories",categories);
        model.addAttribute("links",links);
        model.addAttribute("tags",tags);
        model.addAttribute("hot",true);
        model.addAttribute("actTag",tagService.getTagById(tag_id));
        return view("tag_article");
    }
}

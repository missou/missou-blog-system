package xyz.missou.blog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import xyz.missou.blog.entity.*;
import xyz.missou.blog.service.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController{

    @Resource
    private UserService userService;

    @RequestMapping({"/login/","/login"})
    public String login(HttpServletRequest request,HttpSession session){
        User user= (User) session.getAttribute("user");
        if (user!=null)
            return "redirect:/";
        else
            return view("login");
    }

    @RequestMapping({"/login/verify/","/login/verify"})
    public String loginVerify(HttpServletRequest request, HttpSession session,Model model){
        String account=request.getParameter("account");
        String password=request.getParameter("password");
        User user=userService.verifyUser(account,password);
        if (user == null){
            model.addAttribute("msg","用户名或密码错误，请重新输入！");
            return view("login");
        }else {
            session.setAttribute("user",user);
            return "redirect:/user/login/";
        }
    }
    @RequestMapping({"/signup/","/signup"})
    public String signup(HttpServletRequest request,Model model,@ModelAttribute("msg") String msg){
        model.addAttribute("msg",msg);
        return view("signup");
    }
    @RequestMapping({"/signup/verify/","/signup/verify"})
    public String signupVerify(HttpServletRequest request, RedirectAttributes redirectAttributes, @RequestParam("account") String account, @RequestParam("nickname") String nickname, @RequestParam("password") String password){
        User user=userService.getUserByAccount(account);
        if (user==null){
            redirectAttributes.addAttribute("msg","注册成功，请登录！");
            User newuser=new User();
            newuser.setAccount(account);
            newuser.setNickname(nickname);
            newuser.setPassword(password);
            userService.addUser(newuser);
            return "redirect:/user/login/";
        }else {
            redirectAttributes.addAttribute("msg","该账号已存在，请更换账号后重新注册！");
            return "redirect:/user/signup/";
        }
    }

    @RequestMapping({"/logout/","/logout"})
    public String logout(HttpServletRequest request,HttpSession session){
        session.removeAttribute("user");
        return "redirect:/";
    }

    @RequestMapping({"/nopowertomanage/","/nopowertomanage"})
    public String userNoPower(Model model){
        model.addAttribute("page","usernopower");
        return view("manage/index");
    }
}

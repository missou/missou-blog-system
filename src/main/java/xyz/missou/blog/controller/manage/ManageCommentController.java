package xyz.missou.blog.controller.manage;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import xyz.missou.blog.controller.BaseController;
import xyz.missou.blog.service.CommentService;

import javax.annotation.Resource;

@Controller
@RequestMapping("/manage/comment")
public class ManageCommentController extends BaseController {

    @Resource
    private CommentService commentService;

    @RequestMapping({"/delete/{id}/","/delete/{id}"})
    public String toadd(Model model, @PathVariable("id") Integer id){
        commentService.deleteCommentById(id);
        return "redirect:/manage/comment/";
    }
}

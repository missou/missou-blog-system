package xyz.missou.blog.controller.manage;

import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import xyz.missou.blog.controller.BaseController;
import xyz.missou.blog.entity.*;
import xyz.missou.blog.service.ArticleService;
import xyz.missou.blog.service.ArticleTagService;
import xyz.missou.blog.service.CategoryService;
import xyz.missou.blog.service.TagService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;

@Controller
@RequestMapping("/manage/article")
public class ManageArticleController extends BaseController {

    @Resource
    private CategoryService categoryService;
    @Resource
    private TagService tagService;
    @Resource
    private ArticleService articleService;
    @Resource
    private ArticleTagService articleTagService;

    @RequestMapping({"/toadd/", "/toadd"})
    public String toadd(Model model) {
        ArrayList<Category> categories = categoryService.getAllCategories();
        model.addAttribute("categories", categories);
        model.addAttribute("page","article_toadd");
        return view("manage/index");
    }

    @RequestMapping({"/toupdate/{article_id}/", "/toupdate/{article_id}"})
    public String toupdate(Model model,@PathVariable("article_id") Integer article_id) {
        ArrayList<Category> categories = categoryService.getAllCategories();
        Article article=articleService.getArticleById(article_id);
        model.addAttribute("categories", categories);
        model.addAttribute("article",article);
        model.addAttribute("page","article_toupdate");
        return view("manage/index");
    }

    @ResponseBody
    @RequestMapping(value = {"/add/", "/add"},method = RequestMethod.POST)
    public String add(Model model, HttpSession session, @RequestParam("title") String title, @RequestParam("tags") String tags, @RequestParam("category") Integer category, @RequestParam("summary") String summary, @RequestParam("content") String content) {
        Article article = new Article();
        article.setTitle(title);
        article.setUser((User) session.getAttribute("user"));
        ArrayList<ArticleTag> articleTags = new ArrayList<ArticleTag>();
        ArticleTag articleTag;
        Tag tag;
        for (String tname : tags.trim().split("\\s+")) {
            tag = tagService.getTagByName(tname);
            if (tag == null) {
                tag=new Tag();
                tag.setName(tname);
                tagService.addTag(tag);
            }
            tag = tagService.getTagByName(tname);
            articleTag=new ArticleTag();
            articleTag.setArticle(article);
            articleTag.setTag(tag);
            articleTags.add(articleTag);
        }
        summary=summary.replaceAll("<[^>]*>|\\s+","");
        summary=summary.substring(0,summary.length()<95?summary.length():95)+"...";
        article.setSummary(summary);
        article.setCategory(categoryService.getCategoryByIdMin(category));
        article.setContent(content);
        article.setCreateTime(new Date());
        articleService.addArticle(article);
        articleTagService.addArticleTags(articleTags);
        return "OK";
    }

    @ResponseBody
    @RequestMapping(value = {"/update/", "/update"},method = RequestMethod.POST)
    public String update(Model model, HttpSession session,@RequestParam("id") Integer id, @RequestParam("title") String title, @RequestParam("tags") String tags, @RequestParam("category") Integer category, @RequestParam("summary") String summary, @RequestParam("content") String content) {
        Article article = articleService.getArticleById(id);
        article.setTitle(title);
        ArrayList<ArticleTag> articleTags = new ArrayList<ArticleTag>();
        ArticleTag articleTag;
        Tag tag;
        for (String tname : tags.trim().split("\\s+")) {
            tag = tagService.getTagByName(tname);
            if (tag == null) {
                tag=new Tag();
                tag.setName(tname);
                tag.setSummary("");
                tagService.addTag(tag);
            }
            tag = tagService.getTagByName(tname);
            articleTag=new ArticleTag();
            articleTag.setArticle(article);
            articleTag.setTag(tag);
            articleTags.add(articleTag);
        }
        summary=summary.replaceAll("<[^>]*>|\\s+","");
        summary=summary.substring(0,summary.length()<95?summary.length():95)+"...";
        article.setSummary(summary);
        article.setCategory(categoryService.getCategoryByIdMin(category));
        article.setContent(content);
        articleTagService.deleteByArticleId(id);
        articleTagService.addArticleTags(articleTags);
        articleService.updateArticle(article);
        return "OK";
    }

    @RequestMapping(value = {"/delete/{article_id}/", "/delete/{article_id}"})
    public String delete(Model model, HttpSession session, @PathVariable("article_id") Integer article_id) {
        articleService.deleteArticleById(article_id);
        return "redirect:/manage/article/";
    }

}

package xyz.missou.blog.entity;

import java.util.ArrayList;

/*
* 按月份归档的文章列表
* */
public class MonthArticle {
    private Integer createMonth;
    private ArrayList<Article> articles;

    public Integer getCreateMonth() {
        return createMonth;
    }

    public void setCreateMonth(Integer createMonth) {
        this.createMonth = createMonth;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "MonthArticle{" +
                "createMonth=" + createMonth +
                ", articles=" + articles +
                '}';
    }
}

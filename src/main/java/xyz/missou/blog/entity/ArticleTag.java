package xyz.missou.blog.entity;

public class ArticleTag {
    private Article article;
    private Tag tag;

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "ArticleTag{" +
                ", article=" + article +
                ", tag=" + tag +
                '}';
    }
}

<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 文章 - ${article.title}</title>
    <link href="${basePath}/static/public/editormd/css/editormd.min.css" rel="stylesheet">
    <link href="${basePath}/static/public/editormd/css/editormd.preview.css" rel="stylesheet">
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white border-0 rounded-0 mb-2 py-2 f-15">
                        <li class="breadcrumb-item">
                            <i class="fa fa-home mr-1"></i><a href="${basePath}/">首页</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="${basePath}/category/${article.category.id}/">${article.category.name}</a>
                        </li>
                        <li class="breadcrumb-item active d-none d-md-block" aria-current="page">${article.title}</li>
                        <li class="breadcrumb-item active d-md-none" aria-current="page">当前位置</li>
                    </ol>
                </nav>
                <div class="card rounded-0 border-0" id="article">
                    <div class="card-body px-2 px-md-3 pb-0">
                        <h1 class="card-title text-center font-weight-bold text-info">${article.title}</h1>
                        <hr>
                        <div class="text-center f-13">
                        <span class="mx-2" data-toggle="tooltip" data-placement="bottom"
                              title="发表于${(article.createTime?string("yyyy-MM-dd"))!}">${(article.createTime?string("yyyy-MM-dd"))!}</span>
                            <span class="mx-2">阅读 ${article.pageView}</span>
                            <a class="mx-2 to-com" href="#comment-block">评论 <#if !comments??>
                            0<#else>${comments?size}</#if></a>
                        </div>

                        <!-- 用于显示md编辑器的md格式 -->
                        <div id="md-content" class="markdown-body">
                            <textarea style="display: none;">${article.content}</textarea>
                        </div>

                        <hr>
                        <div class="tag-cloud my-4">
                        <#if tags??>
                            <#list tags as tag>
                                <a href="${basePath}/tag/${tag.id}/" class="tags f-16">${tag.name}</a>
                            </#list>
                        </#if>

                        </div>

                    </div>

                </div>
                <div class="card mt-2 rounded-0 border-0" id="comment-block">

                    <div class="card border-0 rounded-0 f-16" id="editor-block">
                <#if !user??>

                    <div class="card-body text-center m-2 m-md-3 f-16 bg-light" id="no-editor">
                        <div>您尚未登录，请
                            <a class="text-danger" href="${basePath}/user/login/">登录</a> 或
                            <a class="text-danger" href="${basePath}/user/signup/">注册</a> 后评论
                        </div>
                    </div>

                <#else>

                    <div class="card-body text-center m-2 m-md-3 f-16" id="no-editor">
                        <form action="${basePath}/article/${article.id}/addcomment/" id="form-comment" method="POST">
                            <textarea placeholder="请输入你的评论：" class="form-control" rows="5" id="comment" name="content"></textarea>
                            <button type="button" id="btn-comment" class="btn btn-primary m-2">提交评论</button>
                        </form>
                    </div>

                </#if>
                    </div>

                    <div class="card-body p-2 p-md-3 f-17" id="comment-list">
                    <#if !comments??>
                        <div>没有评论，赶快来添加你的评论吧！</div>
                    <#else>
                        <#if comments?size==0>
                        <div>没有评论，赶快来添加你的评论吧！</div>
                        <#else>
                        <ul class="list-unstyled">
                            <div class="mb-3">
                                <strong>有&nbsp;${comments?size}&nbsp;条评论</strong>
                            </div>

                        <#list comments as comment>
                        <hr>
                        <div class="comment-parent pt-2" id="com-256">
                            <li class="media">


                                <img class="avatar"
                                     src="${article.user.avatarUrl}"
                                     alt="${comment.user.nickname}">

                                <div class="media-body ml-2">
                                    <p class="mt-0 mb-1">
                                        <strong>${comment.user.nickname}</strong>
                                    </p>
                                    <p class="small text-muted">
                                        评论于&nbsp;${(comment.createTime?string("yyyy-MM-dd"))}</p>
                                </div>
                            </li>
                            <div class="comment-body"><p>${comment.content}</p></div>
                        </div>
                        </#list>


                        </ul>
                        </#if>
                    </#if>
                    </div>
                </div>
            </div>
            <div id="sidebar" class="col-lg-3">
                <div id="md-toc-container" class="d-none d-lg-block float-right position-fixed">#md-toc-container</div>
            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">
<script src="${basePath}/static/public/editormd/lib/marked.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/prettify.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/raphael.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/underscore.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/sequence-diagram.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/flowchart.min.js"></script>
<script src="${basePath}/static/public/editormd/lib/jquery.flowchart.min.js"></script>
<script src="${basePath}/static/public/editormd/editormd.min.js"></script>
<script>
    var mdcontent;
    $(function () {
        mdcontent = editormd.markdownToHTML("md-content", {
            tocContainer    : "#md-toc-container",
            htmlDecode: "style,script,iframe",  // you can filter tags decode
            emoji: true,
            taskList: true,
            tex: true,  // 默认不解析
            flowChart: true,  // 默认不解析
            sequenceDiagram: true,  // 默认不解析
            codeFold: true
        });
        $("#btn-comment").on("click",function(){
            $.ajax({
                type: 'get',
                data: $('#form-comment').serialize(),
                url: '${basePath}/article/${article.id}/addcomment/',
                cache:false,
                dataType:'text',
                success: function (data) {
                    if("OK"==data){
                        alert("评论成功！");
                        location.reload();
                    }else{
                        alert("评论失败！");
                    }
                },
                error:function(){
                    alert("服务器请求失败！")
                }
            })
        })
    });
</script>
</body>

</html>

<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 归档</title>
    <link href="${basePath}/static/public/editormd/css/editormd.min.css" rel="stylesheet">
    <link href="${basePath}/static/public/editormd/css/editormd.preview.css" rel="stylesheet">
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-8" style="line-height:2">
                <div class="card rounded-0 border-0 p-3 f-16">
                    <h2 class="font-weight-bold">关于网站</h2>
                    <hr style="margin: .1rem;dashed #333;">
                    <ul class="pl-4 ml-2">
                        <li>本网站是一个个人博客网站，主要分享博主的编程学习心得</li>
                        <li>网站主要使用 java SSM 框架 + Bootstrap4 搭建，源码在博主 码云 中， 目前部署在腾讯云</li>
                    </ul>
                    <h2 class="mt-2 font-weight-bold">关于博主</h2>
                    <hr style="margin: .1rem;dashed #333;">
                    <ul class="pl-4 ml-2">
                        <li>博主 码云 地址：<a href="https://gitee.com/missou" target="_blank">https://gitee.com/missou</a></li>
                        <li>联系邮箱：xxxxxx</li>
                    </ul>
                    <h2 class="mt-2 font-weight-bold">功能介绍</h2>
                    <hr style="margin: .1rem;dashed #333;">
                    <ul class="pl-4 ml-2">
                        <li>文章分类、标签、浏览量统计</li>
                        <li>用户登录、注册功能</li>
                        <li>文章查看、评论等功能</li>
                    </ul>
                    <h2 class="mt-2 font-weight-bold">网站支持</h2>
                    <hr style="margin: .1rem;dashed #333;">
                    <ul class="pl-4 ml-2">
                        <li>前端使用 Bootstrap4 支持响应式；图标使用 Font Awesome</li>
                        <li>后端 SpringMVC和Mybatis</li>
                        <li>后台数据库使用 MySQL</li>
                        <li>网站部署使用 Nginx + tomcat</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">

                <!--个人空间-->
                <#include "pages/zone.ftl">

                <!--文章分类-->
                <#include "pages/category.ftl">

                <!--标签云-->
                <#include "pages/tag.ftl">

                <!--友链-->
                <#include "pages/link.ftl">

            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">

</body>

</html>

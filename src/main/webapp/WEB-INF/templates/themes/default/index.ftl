<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle}</title>
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                <!-- 轮播图 -->
                <#include "pages/carousel.ftl">

                <div class="text-secondary font-weight-bold py-2 f-15 choice">
                <#if !hot??>
                    <a class="pb-2 active" href="${basePath}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4" href="${basePath}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                <#else>
                    <#if hot>
                    <a class="pb-2" href="${basePath}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4 active " href="${basePath}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                    <#else>
                    <a class="pb-2 active" href="${basePath}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4 " href="${basePath}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                    </#if>
                </#if>
                </div>
                <!-- 文章列表 -->
                <#include "pages/article.ftl">

            </div>
            <div class="col-lg-4">

                <!--个人空间-->
                <#include "pages/zone.ftl">

                <!--文章分类-->
                <#include "pages/category.ftl">

                <!--标签云-->
                <#include "pages/tag.ftl">

                <!--友链-->
                <#include "pages/link.ftl">

            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">
</body>

</html>
<!doctype html>
<html lang="en">

<head>
    <!-- TDK and ICO -->
    <title>${blogName} - ${blogTitle} - 注册</title>
    <#include "pages/head.ftl">
    <link href="${basePath}/static/public/css/account.css" rel="stylesheet"/>

</head>

<body>

    <#include "pages/navbar.ftl">

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3 px-xl-5">
                <div class="card rounded-0 px-3 px-lg-4">
                    <div class="card-header text-center bg-white py-2">
                        <h3 class="my-1 text-info">注册</h3>
                    </div>
                    <div class="card-body card-login">
                        <#if msg??>
                            <p>${msg}</p>
                        </#if>
                        <form class="signup" id="signup_form" method="post" action="${basePath}/user/signup/verify/">
                            <div class="form-group">
                                <label for="id-account" class="form-control-label  requiredField">
                                    账号<span class="asteriskField">*</span>
                                </label>
                                <div class=""><input type="text" name="account" required placeholder="账号"
                                                     id="id-account" class="textinput textInput form-control"/></div>
                            </div>
                            <div class="form-group"><label for="id-nickname"
                                                           class="form-control-label  requiredField">
                                昵称<span class="asteriskField">*</span> </label>
                                <div class=""><input type="text" name="nickname"
                                                     class="textinput textInput form-control"
                                                     id="id-nickname" required minlength="1" placeholder="昵称"
                                                     autofocus="autofocus"
                                                     maxlength="150"/></div>
                            </div>
                            <div class="form-group"><label for="id-password"
                                                           class="form-control-label  requiredField">
                                密码<span class="asteriskField">*</span> </label>
                                <div class=""><input type="password" name="password" required placeholder="密码"
                                                     id="id-password" class="textinput textInput form-control"/></div>
                            </div>
                            <a href="${basePath}/user/login/">已有账号登录</a>
                            <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">注册</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
        <#include "pages/footer.ftl">
</body>

</html>
<#-- 标签云 -->
<div class="card border-0 rounded-0 px-3 mb-2 mb-md-3" id="tag-card">
    <div class="card-header bg-white px-0">
        <strong><i class="fa fa-tags mr-2 f-17"></i>标&nbsp;签&nbsp;云</strong>
    </div>
    <div class="card-body px-0 py-3">
        <div class="tag-cloud">
            <#list tags as tag>
                <a href="${basePath}/tag/${tag.id}/" class="tags f-16" id="tag-1" <#--title="【${tag.name}】标签下有2篇文章"-->>${tag.name}</a>
            </#list>

        </div>
    </div>
</div>
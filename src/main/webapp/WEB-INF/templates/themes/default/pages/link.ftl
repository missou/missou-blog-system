<#-- 友情链接 -->
<div class="d-none d-lg-block">
    <div class="card border-0 rounded-0 px-3 mb-2 mb-md-3" id="friends-card">
        <div class="card-header bg-white px-0">
            <strong><i class="fa fa-link mr-2 f-17"></i>友情链接</strong>
        </div>
        <div class="card-body px-0 py-3">
            <div class="tool-list">

                <#list links as link>
                <div class="w-50 float-left text-center mb-2">
                    <div class="mx-2">
                        <a href="${link.url}" title="${link.summary}" target="_blank">
                            ${link.name}</a>
                    </div>
                </div>
                </#list>

            </div>
        </div>
    </div>
</div>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <meta name="keywords" content="Python自学,Python爬虫,Django博客,Python web开发,个人博客"> -->

<link rel="shortcut icon" href="${basePath}/static/public/img/favicon.ico" type="image/x-icon"/>
<!-- Bootstrap and font-awesome CSS -->
<link rel="stylesheet" href="${basePath}/static/public/css/bootstrap.min.css">
<link href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="${basePath}/static/public/js/jquery-3.3.1.min.js"></script>
<script src="${basePath}/static/public/js/popper.min.js"></script>
<script src="${basePath}/static/public/js/headroom.min.js"></script>
<script src="${basePath}/static/public/js/bootstrap.min.js"></script>
<!-- blog CSS -->
<link href="${basePath}/static/public/css/base.css" rel="stylesheet">
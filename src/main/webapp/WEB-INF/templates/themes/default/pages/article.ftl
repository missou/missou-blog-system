<#-- 文章列表 -->
<div class="summary-list">
<#list articles as article>
    <div class="media mb-1 mb-sm-2 p-2 p-lg-3">
        <div class="align-self-center mr-2 mr-lg-3 w-25 modal-open">
            <a href="${basePath}/article/${article.id}/" target="_blank">
                <img class="w-100 article-img" src="${article.coverImgUrl}"
                     alt="${article.title}">
            </a>
        </div>
        <div class="media-body">
            <div class="text-muted mb-2 f-12">


                <img class="avatar" src="${article.user.avatarUrl}" alt="missou">

                <span>${article.user.nickname}</span>
                <span><i class="fa fa-calendar-times-o ml-2 mr-1"></i>${(article.createTime?string("yyyy-MM-dd"))!} </span>
            </div>
            <h2 class="mt-0 font-weight-bold text-info f-17">
                <a href="${basePath}/article/${article.id}/" target="_blank">${article.title}</a>
            </h2>
            <p class="d-none d-sm-block mb-2 f-15">${article.summary}</p>
            <#--<p class="d-block d-sm-none mb-2 f-15">这周是9月第2周，在接手同事的容器化代码和自动化部署代码之后发现了自己很大的问题，那就是对 Linux 的脚本和一些常用的...</p>-->
            <div class="text-muted mb-0 f-12">
                <a class="cate" href="${basePath}/category/${article.category.id}/" title="查看当前分类下更多文章">
                    <i class="fa fa-book mr-1"></i>${article.category.name}</a>
                <span><i class="fa fa-eye ml-2 mr-1"></i>${article.pageView}</span>
                <a href="${basePath}/article/${article.id}/#comment-block" target="_blank" title="查看文章评论">
                    <i class="fa fa-comments ml-2 mr-1"></i>${article.comments?size}</a>
            </div>
        </div>
    </div>
</#list>
</div>

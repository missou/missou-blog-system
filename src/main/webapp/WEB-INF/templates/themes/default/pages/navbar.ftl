<#-- 顶部导航栏 -->
<nav class="navbar navbar-expand-md bg-white fixed-top blog-navbar py-md-0 headroom headroom--top">
    <a class="navbar-brand d-md-none d-lg-block" href="${basePath}/">
        <img src="${blogIconPath}" alt="${blogName}" style="width: 115px;height: 40px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-2">

                <a class="nav-link py-md-3 <#if actItem??><#if actItem=="index">active</#if></#if>"
                   href="${basePath}/"><i class="fa fa-home mr-1"></i>首页<span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mr-2">

                <a class="nav-link py-md-3 <#if actItem??><#if actItem=="archive">active</#if></#if>"
                   href="${basePath}/archive/"><i class="fa fa-sitemap mr-1"></i>归档</a>
            </li>
            <li class="nav-item mr-2 ">

                <a class="nav-link py-md-3 <#if actItem??><#if actItem=="about">active</#if></#if>"
                   href="${basePath}/about/"><i class="fa fa-anchor mr-1"></i>关于</a>
            </li>
        <#--<form class="nav-item navbar-form mr-2 py-md-2" role="search" method="get" id="searchform"
              action="/search/">
            <div class="input-group">
                <input type="search" name="q" class="form-control rounded-0 f-15" placeholder="搜索" required="True">
                <div class="input-group-btn">
                    <button class="btn btn-info rounded-0" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div><!-- /input-group &ndash;&gt;
        </form>-->
        </ul>
        <ul class="navbar-nav">

            <#if !user??>
                <li class="nav-item mr-2">
                    <a class="nav-link py-md-3" href="${basePath}/user/login/">登录</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-md-3" href="${basePath}/user/signup/">注册</a>
                </li>
            <#else>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="${user.nickname}，欢迎回来！">
                        <img class="avatar" src="${user.avatarUrl}" alt="${user.nickname}">

                    </a>
                    <div class="dropdown-menu dropdown-menu-right mt-0 rounded-0 border-0"
                         aria-labelledby="navbarDropdown">
                    <#--<a class="dropdown-item pl-3" href="${basePath}/user/profile/">
                        <i class="fa fa-fw fa-user text-info mr-2"></i>个人资料</a>
                    <a class="dropdown-item pl-3" href="/comment/notification/">
                        <i class="fa fa-fw fa-bell text-info mr-2"></i>消息</a>-->
                        <#if user.isManage==1>
                        <a class="dropdown-item pl-3" href="${basePath}/manage/">
                            <i class="fa fa-fw fa-sign-out text-info mr-2"></i>管理</a>
                        </#if>
                        <a class="dropdown-item pl-3" href="${basePath}/user/logout/">
                            <i class="fa fa-fw fa-sign-out text-info mr-2"></i>退出</a>
                    </div>
                </li>
            </#if>
        </ul>
    </div>
</nav>
<!--回到顶部按钮-->
<div class="text-center" id="to-top">
    <i class="fa fa-chevron-up" id="btn-top" title="回到顶部"></i>
</div>



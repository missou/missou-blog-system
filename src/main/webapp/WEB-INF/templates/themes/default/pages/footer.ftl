<script>
    $(function () { var myElement = document.querySelector(".blog-navbar"); var headroom = new Headroom(myElement); headroom.init() });
</script>
<script src="${basePath}/static/public/js/base.js"></script>


<footer class="container-fluid mt-4">
    <div class="card-body text-center px-0 f-16">
        <p class="card-text mb-1">Copyright&nbsp;&copy;&nbsp;2018-2020
            <a href="https://gitee.com/missou" target="_blank" title="前往博主的码云">Missou</a>.&nbsp;Powered&nbsp;by&nbsp;Java.
        </p>
        <p class="mb-0">
            <a href="http://www.miibeian.gov.cn/" target="_blank">鄂ICP备xxxxxxxx号-x</a>
        </p>
    </div>
</footer>
<#--文章分类-->
<div class="card border-0 rounded-0 px-3 mb-2 mb-md-3" id="category-card">
    <div class="card-header bg-white px-0">
        <strong><i class="fa fa-book mr-2 f-17"></i>文章分类</strong>
    </div>

    <ul class="list-group list-group-flush f-16">

        <#if !categories??>
            <li class="list-group-item d-flex justify-content-between align-items-center pr-2 py-2">
                <p>no categories!</p>
            </li>
        <#else>
            <#list categories as category>
            <li class="list-group-item d-flex justify-content-between align-items-center pr-2 py-2">
                <a class="category-item" href="${basePath}/category/${category.id}/"
                   title="${category.summary}">${category.name}</a>
                <span class="badge text-center" title="当前分类下有${category.articles?size}篇文章">${category.articles?size}</span>
            </li>
            </#list>
        </#if>

    </ul>
</div>

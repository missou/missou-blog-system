<#-- 轮播图 -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner">

        <div class="carousel-item">
            <a href="${basePath}/article/54/">
                <img class="w-100" src="http://pic.tendcode.com/article/180418/bsblog.png" alt="在 Linux 服务器上使用 Nginx + Gunicorn 部署 Django 项目的正确姿势">
            </a>
        </div>

        <div class="carousel-item active">
            <a href="${basePath}/article/55/">
                <img class="w-100" src="http://pic.tendcode.com/article/080414/virtualenv.png" alt="Python虚拟环境Virtualenv分别在Windows和Linux上的安装和使用">
            </a>
        </div>

        <div class="carousel-item">
            <a href="${basePath}/article/56/">
                <img class="w-100" src="http://pic.tendcode.com/article/180415/jiandan.png" alt="煎蛋网妹子图爬虫">
            </a>
        </div>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
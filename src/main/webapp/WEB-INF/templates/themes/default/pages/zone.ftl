<#-- 个人空间 -->
<div class="card border-0 rounded-0 px-3 mb-2 mb-md-3 d-none d-lg-block" id="home-card">
    <div class="card-header bg-white px-0">
        <strong><i class="fa fa-paper-plane mr-2 f-17"></i>个人空间</strong>
    </div>
    <div class="card-body px-0 pt-3 pb-1">
        <div class="row text-center">
            <div class="col">
                <a href="https://github.com/missou" target="_blank" title="我的Github">
                    <img style="max-width:40px" alt="博主的github"
                         src="${basePath}/static/public/img/github.png">
                    <p class="mt-2">Github</p>
                </a>
            </div>
            <div class="col">
                <a href="https://gitee.com/missou" target="_blank" title="我的码云">
                    <img style="max-width:40px" alt="博主的码云"
                         src="${basePath}/static/public/img/gitee.png">
                    <p class="mt-2">码云</p>
                </a>
            </div>
            <div class="d-none col">
                <a href="${basePath}/timeline/" target="_blank" title="查看网站建站历程">
                    <img style="max-width:40px" alt="网站的建站历程"
                         src="${basePath}/static/public/img/blog.png">
                    <p class="mt-2">Timeline</p>
                </a>
            </div>
        </div>
    </div>
</div>
<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 归档</title>
    <link href="${basePath}/static/public/editormd/css/editormd.min.css" rel="stylesheet">
    <link href="${basePath}/static/public/editormd/css/editormd.preview.css" rel="stylesheet">
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card border-0 rounded-0 mb-3">
                    <div class="card-body f-16 archive">

                        <ul class="pl-4">
                        <#list monthArticles as monthArticle>
                            <li>${(((monthArticle.createMonth?date("yyyyMM"))!)?string("yyyy年M月"))!} (共 ${monthArticle.articles?size} 篇)
                                <ul class="pl-4">
                                    <#list monthArticle.articles as article>
                                        <li class="text-info">${(article.createTime?string("M月d日"))!}&nbsp;&nbsp;<a href="${basePath}/article/${article.id}/" target="_blank">${article.title}</a></li>
                                    </#list>
                                </ul>
                            </li>
                        </#list>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-lg-4">

                <!--个人空间-->
                <#include "pages/zone.ftl">

                <!--文章分类-->
                <#include "pages/category.ftl">

                <!--标签云-->
                <#include "pages/tag.ftl">

                <!--友链-->
                <#include "pages/link.ftl">

            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">

</body>

</html>

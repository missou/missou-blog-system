<!doctype html>
<html lang="en">
<head>
    <!-- TDK and ICO -->
    <title>${blogName} - ${blogTitle} - 登录</title>
    <#include "pages/head.ftl">
    <link href="${basePath}/static/public/css/account.css" rel="stylesheet"/>

</head>
<body>

<#include "pages/navbar.ftl">

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3 px-xl-5">
                <div class="card rounded-0 px-3 px-lg-4">
                    <div class="card-header text-center bg-white py-2">
                        <h3 class="my-1 text-info">登录</h3>
                    </div>
                    <div class="card-body card-login">
                        <#if msg??>
                            <p>${msg}</p>
                        </#if>
                        <form class="login" method="POST" action="${basePath}/user/login/verify/">

                            <div id="div_id_login" class="form-group">
                                <label for="id_login" class="form-control-label  requiredField">
                                账号<span class="asteriskField">*</span>
                                </label>
                                <div class="">
                                    <input type="text" name="account" required placeholder="用户名或e-mail" autofocus="autofocus" class="textinput textInput form-control" />
                                </div>
                            </div>
                            <div id="div_id_password" class="form-group">
                                <label for="id_password" class="form-control-label  requiredField">
                                    密码<span class="asteriskField">*</span>
                                </label>
                                <div class="">
                                    <input type="password" name="password" required placeholder="密码" class="textinput textInput form-control" />
                                </div>
                            </div>

                            <a class="secondaryAction" href="${basePath}/user/signup/">没有账号？注册一个！</a>
                            <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">登录</button>
                        </form>

                    </div>
                    <#--<div class="text-center mb-5" id="social-login">
                        <div class="login-title">
                            <span>快速登录</span>
                        </div>
                        <div class="login-link">

                            <a class="mx-4" href="/accounts/weibo/login/?next=/search/" title="社交账号登录有点慢，请耐心等候！"><i class="fa fa-weibo fa-2x"></i></a>
                            <a class="mx-4" href="/accounts/github/login/?next=/search/" title="社交账号登录有点慢，请耐心等候！"><i class="fa fa-github fa-2x"></i></a>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</main>
<#include "pages/footer.ftl">
</body>
</html>
<div class="row m-2">
    <div class="col-md-6">
        <h2>分类管理</h2>
    </div>
    <div class="col-md-6">
        <button class="btn btn-primary" data-toggle="modal" data-target="#update-modal-add">新建分类</button>
        <!-- 编辑模态框 -->
        <div class="modal fade" id="update-modal-add">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- 模态框头部 -->
                    <div class="modal-header">
                        <h4 class="modal-title">编辑</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- 模态框主体 -->
                    <div class="modal-body">
                        <form class="signup" id="signup_form" method="post" action="${basePath}/manage/category/add/">
                            <div class="form-group">
                                <label for="id-name" class="form-control-label  requiredField">
                                    名称<span class="asteriskField">*</span>
                                </label>
                                <div class=""><input type="text" name="name" required placeholder="名称"
                                                     id="id-name" class="textinput textInput form-control"/></div>
                            </div>
                            <div class="form-group"><label for="id-nickname"
                                                           class="form-control-label  requiredField">
                                简介<span class="asteriskField">*</span> </label>
                                <div class=""><input type="text" name="summary"
                                                     class="textinput textInput form-control"
                                                     id="id-nickname" required minlength="1" placeholder="简介"
                                                     autofocus="autofocus"
                                                     maxlength="150"/></div>
                            </div>
                            <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">添加分类</button>
                        </form>
                    </div>
                    <!-- 模态框底部 -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>id</th>
        <th>分类名</th>
        <th>简介</th>
        <th>文章篇数</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list categories as category>
    <tr>
        <td>${category.id}</td>
        <td>${category.name}</td>
        <td>${category.summary!}</td>
        <td>${category.articles?size}</td>
        <td>
            <div>
                <button class="btn btn-primary" data-toggle="modal" data-target="#update-modal-update-${category.id}">编辑</button>
                <!-- 编辑模态框 -->
                <div class="modal fade" id="update-modal-update-${category.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- 模态框头部 -->
                            <div class="modal-header">
                                <h4 class="modal-title">编辑</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- 模态框主体 -->
                            <div class="modal-body">
                                <form class="signup" id="signup_form" method="post" action="${basePath}/manage/category/update/${category.id}/">
                                    <div class="form-group">
                                        <label for="id-name" class="form-control-label  requiredField">
                                            名称<span class="asteriskField">*</span>
                                        </label>
                                        <div class=""><input type="text" name="name" value="${category.name}" required placeholder="名称"
                                                             id="id-name" class="textinput textInput form-control"/></div>
                                    </div>
                                    <div class="form-group"><label for="id-nickname"
                                                                   class="form-control-label  requiredField">
                                        简介<span class="asteriskField">*</span> </label>
                                        <div class=""><input type="text" name="summary"
                                                             class="textinput textInput form-control"
                                                             id="id-nickname" required minlength="1" placeholder="简介"
                                                             value="${category.summary}"
                                                             autofocus="autofocus"
                                                             maxlength="150"/></div>
                                    </div>
                                    <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">更新分类</button>
                                </form>
                            </div>
                            <!-- 模态框底部 -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="btn btn-primary" href="${basePath}/manage/category/delete/${category.id}/" onclick="return window.confirm('确定删除吗，删除后将无法恢复？');">删除</a>
            </div>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
<script>
    var msg="";
    <#if msg??>
    msg="${msg}";
    </#if>
    if (msg != "") {
        alert("操作失败："+msg);
    }
</script>

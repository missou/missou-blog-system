<div class="row m-2">
    <div class="col-md-6">
        <h2>文章管理</h2>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="${basePath}/manage/article/toadd/">新建文章</a>
    </div>

</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>id</th>
        <th>标题</th>
        <th>创建时间</th>
        <th>简介</th>
        <th>作者</th>
        <th>分类</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list articles as article>
    <tr>
        <td>${article.id}</td>
        <td>${article.title}</td>
        <td>${(article.createTime?string("yyyy-MM-dd"))!}</td>
        <td>${article.summary}</td>
        <td>${article.user.nickname}</td>
        <td>${article.category.name}</td>
        <td>
            <div>
                <a class="btn btn-primary" href="${basePath}/manage/article/toupdate/${article.id}/">编辑</a>
                <a class="btn btn-primary" href="${basePath}/manage/article/delete/${article.id}/" onclick="return window.confirm('确定删除吗，删除后将无法恢复？');">删除</a>
            </div>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
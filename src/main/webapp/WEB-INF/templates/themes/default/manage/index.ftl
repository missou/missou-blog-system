<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 管理</title>
    <#include "../pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "../pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <!-- 垂直导航栏 -->
                <nav class="navbar bg-light">

                    <ul class="navbar-nav bg-light">
                        <li class="nav-item ml-3 <#if page?index_of("article")!=-1>text-danger</#if>"><a class="nav-link" href="${basePath}/manage/article/">文章管理</a></li>
                        <li class="nav-item ml-3 <#if page?index_of("category")!=-1>text-danger</#if>"><a class="nav-link" href="${basePath}/manage/category/">分类管理</a></li>
                        <li class="nav-item ml-3 <#if page?index_of("tag")!=-1>text-danger</#if>"><a class="nav-link" href="${basePath}/manage/tag/">标签管理</a></li>
                        <li class="nav-item ml-3 <#if page?index_of("user")!=-1>text-danger</#if>"><a class="nav-link" href="${basePath}/manage/user/">用户管理</a></li>
                        <li class="nav-item ml-3 <#if page?index_of("comment")!=-1>text-danger</#if>"><a class="nav-link" href="${basePath}/manage/comment/">评论管理</a></li>
                    </ul>

                </nav>
            </div>
            <div class="container-fluid col-md-10 bg-light">
                <#include "pages/${page}.ftl">
            </div>
        </div>
    </div>
</main>

<#include "../pages/footer.ftl">

</body>

</html>
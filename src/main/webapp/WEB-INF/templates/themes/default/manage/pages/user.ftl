<div class="row m-2">
    <div class="col-md-6">
        <h2>用户管理</h2>
    </div>
    <div class="col-md-6">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#update-modal-add"<#-- href="${basePath}/manage/user/toadd/"-->>新建用户</button>
        <!-- 编辑模态框 -->
        <div class="modal fade" id="update-modal-add">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- 模态框头部 -->
                    <div class="modal-header">
                        <h4 class="modal-title">新建用户</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- 模态框主体 -->
                    <div class="modal-body">
                        <div class="card rounded-0 px-3 px-lg-4">
                            <div class="card-header text-center bg-white py-2">
                                <h3 class="my-1 text-info">新建用户</h3>
                            </div>
                            <div class="card-body card-login">
                                <form class="signup" id="signup_form" method="post" action="${basePath}/manage/user/add/">
                                    <div class="form-group">
                                        <label for="id-account" class="form-control-label  requiredField">
                                            账号<span class="asteriskField">*</span>
                                        </label>
                                        <div class=""><input type="text" name="account" required placeholder="账号"
                                                             id="id-account" class="textinput textInput form-control"/></div>
                                    </div>
                                    <div class="form-group"><label for="id-nickname"
                                                                   class="form-control-label  requiredField">
                                        昵称<span class="asteriskField">*</span> </label>
                                        <div class=""><input type="text" name="nickname"
                                                             class="textinput textInput form-control"
                                                             id="id-nickname" required minlength="1" placeholder="昵称"
                                                             autofocus="autofocus"
                                                             maxlength="150"/></div>
                                    </div>
                                    <div class="form-group"><label for="id-password"
                                                                   class="form-control-label  requiredField">
                                        密码<span class="asteriskField">*</span> </label>
                                        <div class=""><input type="password" name="password" required placeholder="密码"
                                                             id="id-password" class="textinput textInput form-control"/></div>
                                    </div>
                                    <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">添加用户</button>
                                </form>

                            </div>

                        </div>
                    </div>
                    <!-- 模态框底部 -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>id</th>
        <th>账号</th>
        <th>昵称</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list users as user>
    <tr>
        <td>${user.id}</td>
        <td>${user.account}</td>
        <td>${user.nickname}</td>
        <td>
            <div>
                <button class="btn btn-primary" data-toggle="modal" data-target="#update-modal-update-${user.id}">编辑</button>
                <!-- 编辑模态框 -->
                <div class="modal fade" id="update-modal-update-${user.id}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- 模态框头部 -->
                            <div class="modal-header">
                                <h4 class="modal-title">编辑</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- 模态框主体 -->
                            <div class="modal-body">
                                <form class="signup" id="signup_form" method="post" action="${basePath}/manage/user/update/${user.id}/">
                                    <div class="form-group">
                                        <label for="id-account" class="form-control-label  requiredField">
                                            账号<span class="asteriskField">*</span>
                                        </label>
                                        <div class=""><input type="text" name="account" value="${user.account}" required placeholder="账号"
                                                             id="id-account" class="textinput textInput form-control"/></div>
                                    </div>
                                    <div class="form-group"><label for="id-nickname"
                                                                   class="form-control-label  requiredField">
                                        昵称<span class="asteriskField">*</span> </label>
                                        <div class=""><input type="text" name="nickname"
                                                             class="textinput textInput form-control"
                                                             id="id-nickname" required minlength="1" placeholder="昵称"
                                                             value="${user.nickname}"
                                                             autofocus="autofocus"
                                                             maxlength="150"/></div>
                                    </div>
                                    <button class="pull-right btn btn-info btn-sm rounded-0" type="submit">更新用户</button>
                                </form>
                            </div>
                            <!-- 模态框底部 -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                            </div>
                        </div>
                    </div>
                </div>
                <#if user.isManage==0>
                    <a class="btn btn-primary" href="${basePath}/manage/user/delete/${user.id}/" onclick="return window.confirm('确定删除吗，删除后将无法恢复？');">删除</a>
                </#if>
            </div>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
<script>
    var msg="";
    <#if msg??>
    msg="${msg}";
    </#if>
    if (msg != "") {
        alert("操作失败："+msg);
    }
</script>

<link href="${basePath}/static/public/editormd/css/editormd.min.css" rel="stylesheet">

<form id="form-article" class="form-inline" action="${basePath}/manage/article/add/" method="post">
    <div class="form-inline" style="width: 90%;margin-left: 5%">
        <label for="title">文章标题：</label><input type="text" class="form-control mx-3 my-1" id="title" name="title" placeholder="文章标题：">
        <label for="tags">标签(多个标签用空格分隔)：</label><input type="text" class="form-control mx-3 my-1" id="tags" name="tags" placeholder="标签：">
        <br/>
        <label for="category">文章分类：</label>
        <select class="form-control mx-3 my-1" id="category" name="category">
            <#list categories as category>
                <option value="${category.id}">${category.name}</option>
            </#list>
        </select>
        <button type="button" id="submit-article" class="btn btn-primary mx-3 my-1">发布文章</button>
    </div>
    <div class="editormd" id="md-article-content">
        <textarea class="editormd-markdown-textarea" name="content"></textarea>
        <!-- 第二个隐藏文本域，用来构造生成的HTML代码，方便表单POST提交，这里的name可以任意取，后台接受时以这个name键为准 -->
        <textarea id="summary-text" class="editormd-html-textarea" name="summary"></textarea>
    </div>
</form>
<script src="${basePath}/static/public/editormd/editormd.min.js"></script>
<script>
    var myeditor;
    $(function() {
        myeditor=editormd("md-article-content", {
            width   : "90%",
            height  : 640,
            saveHTMLToTextarea : true,
            path    : "${basePath}/static/public/editormd/lib/",
            /**上传图片相关配置如下*/
            imageUpload : true,
            imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "${basePath}/pic/upload/",//后端的上传图片地址
            syncScrolling : "single",
        });
    });
    $("#submit-article").click(function(){
        var targetUrl = $('#form-article').attr("action");
        var data = $('#form-article').serialize();
        $.ajax({
            type:'post',
            url:targetUrl,
            cache: false,
            data:data,
            dataType:'text',
            success: function (data) {
                if("OK"==data){
                    alert("发布文章成功！");
                    location.href="${basePath}/manage/article/";
                }else{
                    alert("发布文章失败！");
                }
            },
            error:function(){
                alert("服务器请求失败")
            }
        });
    });
</script>
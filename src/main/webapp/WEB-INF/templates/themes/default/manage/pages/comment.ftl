<div class="row m-2">
    <div class="col-md-6">
        <h2>评论管理</h2>
    </div>
</div>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>id</th>
        <th>内容</th>
        <th>创建时间</th>
        <th>所属文章</th>
        <th>作者</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list comments as comment>
    <tr>
        <td>${comment.id}</td>
        <td>${comment.content}</td>
        <td>${(comment.createTime?string("yyyy-MM-dd"))!}</td>
        <td>${comment.article.title}</td>
        <td>${comment.user.nickname}</td>
        <td>
            <div>
                <a class="btn btn-primary" href="${basePath}/manage/comment/delete/${comment.id}/" onclick="return window.confirm('确定删除吗，删除后将无法恢复？');">删除</a>
            </div>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 标签 - ${actTag.name}</title>
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                <div class="description bg-white px-3 pt-3 pb-1">
                    <p class="float-right mb-0">共<span class="mx-2 text-info">${articles?size}</span>篇</p>
                    <h1 class="f-17"><strong>文章标签：${actTag.name}</strong></h1>
                    <p class="f-16">${(actTag.summary)!}</p>
                </div>

                <div class="text-secondary font-weight-bold py-2 f-15 choice">
                <#if !hot??>
                    <a class="pb-2 active" href="${basePath}/tag/${actTag.id}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4" href="${basePath}/tag/${actTag.id}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                <#else>
                    <#if hot>
                    <a class="pb-2" href="${basePath}/tag/${actTag.id}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4 active " href="${basePath}/tag/${actTag.id}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                    <#else>
                    <a class="pb-2 active" href="${basePath}/tag/${actTag.id}/">
                        <i class="fa fa-bars mr-1"></i>时间排序</a>
                    <a class="pb-2 ml-4 " href="${basePath}/tag/${actTag.id}/hot/">
                        <i class="fa fa-fire mr-1"></i>热度排序</a>
                    </#if>
                </#if>
                </div>
                <!-- 文章列表 -->
                <#include "pages/article.ftl">

            </div>
            <div class="col-lg-4">

                <!--个人空间-->
                <#include "pages/zone.ftl">

                <!--文章分类-->
                <#include "pages/category.ftl">

                <!--标签云-->
                <#include "pages/tag.ftl">

                <!--友链-->
                <#include "pages/link.ftl">

            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">

</body>

</html>
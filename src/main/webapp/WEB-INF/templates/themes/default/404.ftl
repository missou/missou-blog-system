<!doctype html>
<html lang="en">

<head>
    <title>${blogName} - ${blogTitle} - 404</title>
    <#include "pages/head.ftl">
</head>

<body>
<!--导航开始-->
<#include "pages/navbar.ftl">
<!--导航结束-->

<!--主要内容块-->
<main>
    <div class="container">
        <div class="row errorpage">
            <div class="col align-self-center error-img text-center">
                <div class="errorimg">
                    <img class="img-fluid" src="${basePath}/static/public/img/404.png">
                </div>
                <div class="errormsg">
                    <h3>哇哦！你好像访问到了异次元世界...</h3>
                    <a href="javascript:history.go(-1);">返回地球</a>
                </div>
            </div>
        </div>
    </div>
</main>

<#include "pages/footer.ftl">

</body>

</html>
